## A golang library for controlling Futaba C2CIG equiped VFD modules

There are some pretty cheap and decent VFD modules made by Futaba out there. They don't seem to have too good library coverage and the datasheets for specific models are sometimes hard to find...

This golang library handles them via SPI with periph library.

### Protocol

The Futaba C2CIG VFD character generator uses a SPI-like serial protocol. Data is send LSB first. Periph doesn't seem to support LSB first so we use bits.Reverse8 for it. Data is clocked in on the rising edge of CLK, which corresponds to SPI mode3. Maximum clock frequency is given as 500kHz. All commands bar CGRAM and DCRAM writes are always 2 bytes long. First byte sent is the command byte, followed by data bytes.

#### Command byte

| Command 			| B7  | B6 | B5 | B4 | B3 | B2 | B1 | B0 |
|:-:				|:-:  |:-: |:-: |:-: |:-: |:-: |:-: |:-: |
| DCRAM_A write 	|  0  | 0  | 0  | X4 | X3 | X2 | X1 | X0 |
| DCRAM_B write 	|  0  | 0  | 1  | X4 | X3 | X2 | X1 | X0 |
| CGRAM write 		|  0  | 1  | 0  |    |    | Y2 | Y1 | Y0 |
| Digits set 		|  1  | 1  | 1  | 0  | 0  | 0  |    |    |
| Dimming set 		|  1  | 1  | 1  | 0  | 0  | 1  |    |    |
| Gray-level set 	|  1  | 0  | 1  |    |    | J2 | J1 | J0 |
| Gray-level on/off	|  1  | 1  | 0  | X4 | X3 | X2 | X1 | X0 |
| Display light set |  1  | 1  | 1  | 0  | 1  | 0  | LS | HS |

Attributes inside command byte:
* Empty spaces: do not care
* X0 - X4: Character address (0-24)
* Y0 - Y2: Custom glyph address (0-7)
* J0 - J2: Row select (0-1 on my datasheet...)
* LS: 1 - All output off, 0 - normal mode
* HS: 1 - All output on, 0 - normal mode

Commands in brief:
* DCRAM writes are followed by one or more bytes of character data, address is automatically incremented. A is the top row, B lower row. On single row VFDs the only row seems to be B
* CGRAM writes are followed by one or more 5 byte long character maps. The bits 0-6 of each byte form the 5x7 character bitmap
* Digits set command is followed by byte setting the number of characters per line on the VFD, -1, so 8 character display sends 0x07 as second byte
* Dimming set is followed by a byte for the dimming value, 0-240 (values above 240 are treated as 240). 0 is off, 240 full brightness
* Gray-level stuff allows you to set a dimming amount per row for "gray" glyphs.
  * set command is followed by the dimming amount byte as in the dimming set command
  * on/off is followed by a byte with B0 affecting row A and B1 affecting row B
* Display light set can be used to turn display off or to light all segments