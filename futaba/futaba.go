package futaba

import (
	"math/bits"
	"periph.io/x/periph/conn/gpio"
	"periph.io/x/periph/conn/physic"
	"periph.io/x/periph/conn/spi"
	"periph.io/x/periph/conn/spi/spireg"
	"time"
)

const (
	cmdDCRAMA    = 0x00 // Bits 0-4 contain address (0-23)
	cmdDCRAMB    = 0x20 // Biys 0-4 contain address (0-23)
	cmdCGRAM     = 0x40 // Bits 0-2 contain address (0-7)
	cmdDigits    = 0xE0
	cmdDimming   = 0xE4
	cmdGrayLevel = 0xA0 // Bits 0-2 contain settings
	cmdGrayOnOff = 0xC0 // Bits 0-4 contain settings
	cmdLight     = 0xE8 // Bits 0, 1 contain settings

	lightLS = 0x02 // not set == normal, set == all off
	lightHS = 0x01 // not set == normal, set == all on

	resetPulse = 3 * time.Microsecond // Datasheet gives minimum of 2 microseconds
	readyTime  = 3 * time.Millisecond // Datasheet gives max of 3 milliseconds until ready after reset
)

// VFD struct for an instance of Futaba C2CIG serial controlled VFD module
type VFD struct {
	conn     spi.Conn    // SPI connection
	resetPin gpio.PinOut // Reset line
	csPin    gpio.PinOut // Chip select pin
}

// InitVFD initializes a futaba VFD
func InitVFD(spiDev string, csPin, resetPin gpio.PinOut, digits int) (*VFD, error) {
	p, err := spireg.Open(spiDev)
	if err != nil {
		return nil, err
	}

	// Convert the spi.Port into a spi.Conn so it can be used for communication.
	c, err := p.Connect(physic.KiloHertz*50, spi.Mode3, 8)
	if err != nil {
		p.Close()
		return nil, err
	}

	vfd := VFD{
		conn:     c,
		resetPin: resetPin,
		csPin:    csPin,
	}

	vfd.Reset()
	vfd.digits(digits)
	vfd.Dimming(0x50)

	return &vfd, nil
}

// PrintA writes to the DCRAM 'A' starting from given offset
func (vfd *VFD) PrintA(start int, text []byte) {
	addr := byte(start & 0x1F)
	data := []byte{cmdDCRAMA + addr}
	for _, c := range text {
		data = append(data, c)
	}
	vfd.send(data)
}

// PrintB writes to the DCRAM 'B' starting from given offset
func (vfd *VFD) PrintB(start int, text []byte) {
	addr := byte(start & 0x1F)
	data := []byte{cmdDCRAMB + addr}
	for _, c := range text {
		data = append(data, c)
	}
	vfd.send(data)
}

// Reset resets the VFD display via the reset pin
func (vfd *VFD) Reset() {
	vfd.resetPin.Out(gpio.Low)
	time.Sleep(resetPulse)
	vfd.resetPin.Out(gpio.High)
	time.Sleep(readyTime)
}

// AllOn turns on all segments
func (vfd *VFD) AllOn() {
	vfd.send([]byte{cmdLight + lightHS, 0x00})
}

// Off turns all segments off
func (vfd *VFD) Off() {
	vfd.send([]byte{cmdLight + lightLS, 0x00})
}

// Normal sets the segments into normal mode, where they are controlled by the glyphs
func (vfd *VFD) Normal() {
	vfd.send([]byte{cmdLight, 0x00})
}

// Dimming sets the dimming level of the VFD
func (vfd *VFD) Dimming(dim byte) {
	vfd.send([]byte{cmdDimming, dim})
}

// digits sets the number of digits on the VFD
func (vfd *VFD) digits(digits int) {
	d := byte((digits - 1) & 0xF)
	vfd.send([]byte{cmdDigits, d})
}

// TODO rely on SPI to handle CS?
func (vfd *VFD) send(data []byte) {
	vfd.csPin.Out(gpio.Low)
	for _, b := range data {
		rev := byte(bits.Reverse8(uint8(b)))
		if err := vfd.conn.Tx([]byte{rev}, nil); err != nil {
			panic(err)
		}
	}
	vfd.csPin.Out(gpio.High)
}
