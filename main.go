package main

import (
	"fmt"
	"gitlab.com/Depili/vfd/futaba"
	"log"
	"periph.io/x/periph/conn/spi/spireg"
	"periph.io/x/periph/host"
	"periph.io/x/periph/host/rpi"
	"strings"
	"time"
)

func main() {
	// Make sure periph is initialized.
	if _, err := host.Init(); err != nil {
		log.Fatal(err)
	}

	fmt.Print("SPI ports available:\n")
	for _, ref := range spireg.All() {
		fmt.Printf("- %s\n", ref.Name)
		if ref.Number != -1 {
			fmt.Printf("  %d\n", ref.Number)
		}
		if len(ref.Aliases) != 0 {
			fmt.Printf("  %s\n", strings.Join(ref.Aliases, " "))
		}
	}

	vfd, _ := futaba.InitVFD("/dev/spidev0.0", rpi.P1_26, rpi.P1_15, 8)

	log.Printf("Lighting everything")
	vfd.AllOn()

	time.Sleep(1 * time.Second)
	log.Printf("Normal mode")
	vfd.Normal()

	log.Printf("Sending some characters")
	vfd.PrintB(0, []byte("Abcdefgh"))
	time.Sleep(1 * time.Second)

	vfd.PrintB(0, []byte("Kissa2  "))
	time.Sleep(1 * time.Second)

	t := "0:17:59"
	data := string([]byte{0x8})
	data += t
	vfd.PrintB(0, []byte(data))
	time.Sleep(1 * time.Second)

	vfd.PrintB(0, []byte{0x09, 0x30, 0x3A, 0x31, 0x37, 0x3A, 0x35, 0x39})
	time.Sleep(1 * time.Second)

	vfd.PrintB(0, []byte{0x0B, 0x30, 0x3A, 0x31, 0x37, 0x3A, 0x35, 0x39})
	time.Sleep(1 * time.Second)

	vfd.PrintB(0, []byte{0x0E, 0x30, 0x3A, 0x31, 0x37, 0x3A, 0x35, 0x39})
	time.Sleep(1 * time.Second)

	vfd.PrintB(0, []byte{0x0F, 0x30, 0x3A, 0x31, 0x37, 0x3A, 0x35, 0x39})
	time.Sleep(1 * time.Second)

	vfd.Reset()
}
